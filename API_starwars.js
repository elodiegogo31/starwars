


//var api = 'https://www.swapi.co/api/people';
var api = 'api-starwars.json';


$.getJSON(api,function(data){	
	// console.log(data)
	//	console.log(data.results)
		//var i =0;
		//console.log(data.results[i])

	for(var i=0; i<data.results.length; i++){
		var character = data.results[i];
	//	console.log(character)


	//	var list_of_char = {char:name};
		var template = "<option class='char_option' data-value='{{character}}'>{{name}}</option>";                 // "<option>{{list_of_char}}</option>";
		var output= Mustache.render(template, {
			name: character.name,
			character: JSON.stringify(character)
		});     
		                        // template, list_of_char
		$("#character").append(output);


		//var characters_options = data.results;
		//console.log(character_option);


	}
});




$("#character").on("change", function(){

	console.log( $(this).find(":selected").data('value') );

	var user_select = $(this).find(":selected").data('value');

	var character_info = {
		name : user_select.name,
		height: user_select.height,
		weight: user_select.mass,
		hair_color: user_select.hair_color,
		skin_color: user_select.skin_color,
		eye_color: user_select.eye_color,
		birth_year: user_select.birth_year,
		gender: user_select.gender,
		homeworld: user_select.homeworld,
		films: user_select.films,
		species: user_select.species,
		vehicles: user_select.vehicles,
		starships: user_select.starships,
		created: user_select.created,
		edited: user_select.edited,
		url: user_select.url,


	}

	var template_chara = 
	"<h2>name: {{name}}</h2>"+
	"<p>height: {{height}}</p>"+
	"<p>weight: {{weight}}</p>"+
	"<p>hair color: {{hair_color}}</p>"+
	"<p>skin color: {{skin_color}}</p>"+
	"<p>eye color: {{eye_color}}</p>"+
	"<p>year of birth: {{birth_year}}</p>"+
	"<p>gender: {{gender}}</p>"+
	"<p>homeworld: {{homeworld}}</p>"+
	"<p>films: {{films}}</p>"+
	"<p>species: {{species}}</p>"+
	"<p>vehicles: {{vehicles}}</p>"+
	"<p>starships: {{starships}}</p>"+
	"<p>profil created on: {{created}}</p>"+
	"<p>profil edited on: {{edited}}</p>"+
	"<p>url: {{url}}</p>";

	var info_chara = Mustache.render(template_chara, character_info)
	$('#inform_user_about_character').append(info_chara);

});






//var api_movies = 'https://swapi.co/api/films/';
var api_movies = 'api-starwars-movie.json';

$.getJSON(api_movies,function(data){	

	for(var i=0; i<data.results.length; i++){
		var movie = data.results[i];
	
		var template = "<option class='char_option' data-value='{{movie}}'>{{name}}</option>";          
		var output= Mustache.render(template, {
			name: movie.title,
			movie: JSON.stringify(movie)
		});     
		                       
		$("#movie").append(output);

	}
});




$("#movie").on("change", function(){

	console.log( $(this).find(":selected").data('value') );

	var user_select = $(this).find(":selected").data('value');

	var movie_info = {
		title : user_select.title,
		episode_id: user_select.episode_id,
		opening_crawl: user_select.opening_crawl,
		director: user_select.director,
		producer: user_select.producer,
		release_date: user_select.release_date,
		characters: user_select.characters,
		planets: user_select.planets,
		starships: user_select.starships,
		vehicles: user_select.vehicles,
		species: user_select.species,
		created: user_select.created,
		edited: user_select.edited,
		url: user_select.url,


	}

	var template_movie = 
	"<h2>title: {{title}}</h2>"+
	"<p>episode_id: {{episode_id}}</p>"+
	"<p>opening_crawl: {{opening_crawl}}</p>"+
	"<p>director: {{director}}</p>"+
	"<p>producer: {{producer}}</p>"+
	"<p>release_date: {{release_date}}</p>"+
	"<p>characters: {{characters}}</p>"+
	"<p>planets: {{planets}}</p>"+
	"<p>starships: {{starships}}</p>"+
	"<p>vehicles: {{vehicles}}</p>"+
	"<p>species: {{species}}</p>"+
	"<p>profil created on: {{created}}</p>"+
	"<p>profil edited on: {{edited}}</p>"+
	"<p>url: {{url}}</p>";

	var info_movie = Mustache.render(template_movie, movie_info)
	$('#inform_user_about_movie').append(info_movie);

});






//var api_planet = 'https://www.swapi.co/api/planets';
var api_planets = 'api-starwars-planet.json';


$.getJSON(api_planets,function(data){	

	for(var i=0; i<data.results.length; i++){
		var planet = data.results[i];
	
		var template = "<option class='char_option' data-value='{{planet}}'>{{name}}</option>";          
		var output= Mustache.render(template, {
			name: planet.name,
			planet: JSON.stringify(planet)
		});     
		                       
		$("#planet").append(output);

	}
});




$("#planet").on("change", function(){

	console.log( $(this).find(":selected").data('value') );

	var user_select = $(this).find(":selected").data('value');

	var planet_info = {
		name : user_select.name,
		rotation_period: user_select.rotation_period,
		orbital_period: user_select.orbital_period,
		diameter: user_select.diameter,
		climate: user_select.climate,
		gravity: user_select.gravity,
		terrain: user_select.terrain,
		surface_water: user_select.surface_water,
		population: user_select.population,
		residents: user_select.residents,
		films: user_select.films,
		created: user_select.created,
		edited: user_select.edited,
		url: user_select.url,


	}

	var template_planet = 
	"<h2>name: {{name}}</h2>"+
	"<p>rotation_period: {{rotation_period}}</p>"+
	"<p>orbital_period: {{orbital_period}}</p>"+
	"<p>diameter: {{diameter}}</p>"+
	"<p>climate: {{climate}}</p>"+
	"<p>gravity: {{gravity}}</p>"+
	"<p>terrain: {{terrain}}</p>"+
	"<p>surface_water: {{surface_water}}</p>"+
	"<p>population: {{population}}</p>"+
	"<p>residents: {{residents}}</p>"+
	"<p>films: {{films}}</p>"+
	"<p>profil created on: {{created}}</p>"+
	"<p>profil edited on: {{edited}}</p>"+
	"<p>url: {{url}}</p>";

	var info_planet = Mustache.render(template_planet, planet_info)
	$('#inform_user_about_planet').append(info_planet);

});








/*
	var 

		var set_of_data = {
			name : data.results[i].name,
			height : data.results[i].height,
			weight : data.results[i].mass,
			hair_color : data.results[i].hair_color,
			skin_color : data.results[i].skin_color,
			eye_color : data.results[i].eye_color,
			birth_year : data.results[i].birth_year,
			gender : data.results[i].gender,
			homeworld : data.results[i].homeworld,
			films : data.results[i].films,
			species : data.results[i].species,
			vehicles : data.results[i].vehicles,
			starships : data.results[i].starships,
			created : data.results[i].created,
			edited : data.results[i].edited,
			url : data.results[i].url,
		}

		var template = "<div>{{name}}</div>";


		var get_the_info_out_into_the_world = Mustache.render(template, set_of_data);
	 	$('#inform_user').append(get_the_info_out_into_the_world);

*/